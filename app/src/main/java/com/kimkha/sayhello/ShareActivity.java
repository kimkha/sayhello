package com.kimkha.sayhello;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.plus.PlusShare;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by kimkha on 7/12/14.
 */
public class ShareActivity extends ActionBarActivity implements View.OnClickListener {
    private Button mShareButton;

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {

            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }

        mShareButton = (Button) findViewById(R.id.share_button);
        mShareButton.setOnClickListener(this);
        mShareButton.setEnabled(true);

        //finish();
    }

    private void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            String url = null;
            try {
                url = "http://parsetoblogger.appspot.com/parse?url=" + URLEncoder.encode(sharedText, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            new FetchTask().execute(url);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.share_button:
                PlusShare.Builder builder = new PlusShare.Builder(this);

                builder.setContentUrl(Uri.parse("http://www.kimkha.com"));

                startActivityForResult(builder.getIntent(), 0);
                break;
        }
    }

    class FetchTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... urls) {
            HttpClient mClient= new DefaultHttpClient();
            HttpGet get = new HttpGet(urls[0]);
            try {
                HttpResponse res = mClient.execute(get);
                res.getEntity().consumeContent();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
