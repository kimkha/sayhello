package com.kimkha.sayhello;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * @author kimkha
 * @version 0.2
 * @since 5/6/15
 */
public class MyWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}
